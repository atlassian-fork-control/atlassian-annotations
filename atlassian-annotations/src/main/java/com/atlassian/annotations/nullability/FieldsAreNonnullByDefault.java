package com.atlassian.annotations.nullability;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Similar to {@link javax.annotation.ParametersAreNonnullByDefault}, but applies to fields of classes.
 *
 * @since 2.2.0
 */
@Documented
@NonNull
@TypeQualifierDefault(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldsAreNonnullByDefault {
}
