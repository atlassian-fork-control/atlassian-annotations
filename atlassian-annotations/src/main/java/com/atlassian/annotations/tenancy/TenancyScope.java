package com.atlassian.annotations.tenancy;

import com.atlassian.annotations.Internal;

/**
 * An expression of how a multi-tenanted environment affects the behaviour of the annotated component.
 *
 * @since v0.20
 */
@Internal
public enum TenancyScope {
    /**
     * The definition of 'tenanted' is dependent on where it is applied:
     *
     * <ul>
     *     <li>
     *          <strong>Methods</strong>: Indicates that the method uses data access patterns that require knowledge of the
     *          current tenant.
     *     </li>
     *     <li>
     *          <strong>Fields and types</strong>: Indicates that the component contains or references tenant specific data
     *          (either directly or indirectly).
     *     </li>
     * </ul>
     * <p>
     * Both definitions imply that the method or component must only be used within tenant request contexts.
     * </p>
     */
    TENANTED,

    /**
     * The definition of 'tenantless' is dependent on where it is applied:
     *
     * <ul>
     *     <li>
     *         <strong>Methods</strong>: Indicates that the method only accesses data that is universal across all tenants. Tenantless
     *         methods may be called safely even outside of a tenanted request context. Tenantless methods must avoid calls resulting
     *         in tenant specific data access. Calling {@link #TENANTED} methods from {@code TENANTLESS} methods is generally an error,
     *         since if this service calls another service that requires a tenant, then this service transitively requires one as well.
     *     </li>
     *     <li>
     *          <strong>Fields and types</strong>: Indicates that the component does not contain references to tenant specific
     *          data (either directly or indirectly).
     *     </li>
     * </ul>
     */
    TENANTLESS,

    /**
     * Explicitly marks that this is a component whose safety in a multi-tenanted environment has not yet
     * been evaluated or that is known to require additional work.
     * <p>
     * This is intended to be used temporarily to help track remaining multi-tenancy work.  It should not
     * be assigned in new code unless properly resolving the problem is blocked by some external dependency.
     * </p>
     */
    UNRESOLVED,


    /**
     * Explicitly marks that tenancy scope concerns are not relevant for this component and that it should be exempt from static analysis rules.
     * <p>
     * This is intended for use on components that code analysis tools are likely to flag as suspicious, but are naturally safe due to how they are used.
     * </p>
     */
    SUPPRESS;
}