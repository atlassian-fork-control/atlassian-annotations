package com.atlassian.api.analyser;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 * This class exports the API definition of the current artifact as an XML file
 * <p/>
 * This XML signature file can be used as a baseline to check for API/SPI breakages in later versions of the artifact.
 * <p/>
 * This uses ASM to process the classes in the jar file.
 *
 * @goal extract-api
 * @phase verify
 */
public class ExtractApiMojo extends AbstractMojo {

    /**
     * @component
     */
    private org.apache.maven.artifact.resolver.ArtifactResolver resolver;

    /**
     * @component
     */
    private ArtifactFactory artifactFactory;

    /**
     * Location of the local repository.
     *
     * @parameter expression="${localRepository}"
     * @readonly
     * @required
     */
    protected ArtifactRepository localRepository;

    /**
     * Location of the local repository.
     *
     * @parameter expression="${checkApi.baselineXML}"
     * @required
     */
    protected String baselineXml;

    /**
     * @parameter default-value="${project}"
     */
    private org.apache.maven.project.MavenProject mavenProject;

    public void execute() throws MojoExecutionException, MojoFailureException {

        Artifact currentArtifact = mavenProject.getArtifact();
        File currentJarFile = currentArtifact.getFile();
        Document document = loadApiJar(currentJarFile);

        // If all went well we can output the document
        // Pretty print the document to System.out
        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = null;
        try {
            Writer out = new FileWriter(baselineXml);
            writer = new XMLWriter(out, format);
            writer.write(document);
            writer.close();
        } catch (UnsupportedEncodingException ex) {
            throw new MojoExecutionException("Error Writing XML document: " + baselineXml, ex);
        } catch (IOException ex) {
            throw new MojoExecutionException("Error Writing XML document: " + baselineXml, ex);
        }
    }

    private Document loadApiJar(File jarFile) {
        Document document = DocumentHelper.createDocument();
        Element productElement = document.addElement("product");

        ProductApiJarScanner scanner = new ProductApiJarScanner();
        try {
            scanner.scanJar(productElement, jarFile.getAbsolutePath(), new FileInputStream(jarFile), getLog());
        } catch (FileNotFoundException ex) {
            getLog().error("Error opening jar file: " + jarFile, ex);
        }
        return document;
    }

}
